package be.foreach.stash.hook;

import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.StringOutputHandler;
import com.atlassian.utils.process.Watchdog;

import java.io.InputStream;

public class GitStringOutputHandler implements com.atlassian.stash.scm.CommandOutputHandler<java.lang.String> {
    public GitStringOutputHandler() {
    }

    public String getOutput() {
        String output = outputHandler.getOutput();
        if (output != null && output.trim().isEmpty())
            output = null;
        return output;
    }

    public void process(InputStream output)
            throws ProcessException {
        outputHandler.process(output);
    }

    public void complete()
            throws ProcessException {
        outputHandler.complete();
    }

    public void setWatchdog(Watchdog watchdog) {
        outputHandler.setWatchdog(watchdog);
    }

    private final StringOutputHandler outputHandler = new StringOutputHandler();
}
