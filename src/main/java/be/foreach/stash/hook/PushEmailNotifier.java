package be.foreach.stash.hook;

import com.atlassian.stash.exception.MailSizeExceededException;
import com.atlassian.stash.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.atlassian.stash.user.StashAuthenticationContext;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PushEmailNotifier implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator {

    private static final Logger LOG = LoggerFactory.getLogger( PushEmailNotifier.class );

    private final SmtpNotificationRenderer smtpNotificationRenderer;
    private final StashAuthenticationContext stashAuthenticationContext;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    private final String defaultTemplate;
    private final String defaultFromTemplate;
    private final String defaultSubjectTemplate;

    public PushEmailNotifier(final SmtpNotificationRenderer smtpNotificationRenderer, final StashAuthenticationContext stashAuthenticationContext,
                             final ApplicationPropertiesService applicationPropertiesService, final GitCommandBuilderFactory gitCommandBuilderFactory) throws IOException {
        this.smtpNotificationRenderer = smtpNotificationRenderer;
        this.stashAuthenticationContext = stashAuthenticationContext;
        this.applicationPropertiesService = applicationPropertiesService;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;

        Velocity.setProperty( RuntimeConstants.VM_LIBRARY_AUTORELOAD, true );

        defaultTemplate = getResource( "notifyemail.vm" );
        defaultFromTemplate = getResource( "notifyemailfrom.vm" );
        defaultSubjectTemplate = getResource( "notifyemailsubject.vm" );
    }

    private String getResource( String templateName ) throws IOException {
        InputStream is = PushEmailNotifier.class.getClassLoader().getResourceAsStream( "templates/" + templateName );
        if( is != null ) {
            return readTextFile( is, "UTF-8" );
        } else {
            return "Could not find resource";
        }
    }

    public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges) {
        LOG.debug( "refchanges size:" + refChanges.size() );
        String mailTo = context.getSettings().getString("mailToAddress");

        if (mailTo != null) {
            List<String> emailAddresses = new ArrayList<String>();
            if( mailTo.indexOf( ';' ) >= 0 ) {
                String[] splitEmails = mailTo.split( ";" );
                for (String splitEmail : splitEmails) {
                    emailAddresses.add( StringUtils.trim( splitEmail ) );
                }
            } else {
                emailAddresses.add( StringUtils.trim( mailTo ) );
            }

            Repository repository = context.getRepository();
            for (RefChange refChange : refChanges) {
                sendEmailForRefChange(emailAddresses, repository, refChange, context.getSettings());
            }
        }
    }

    private void sendEmailForRefChange(List<String> emailAddresses, Repository repository, RefChange refChange, Settings settings ) {
        LOG.info("Checking ref:{}, type:{}, from:{}, to:{}", refChange.getRefId(), refChange.getType(), refChange.getFromHash(), refChange.getToHash() );
        try {
            renderAndSendMail(emailAddresses, repository, refChange, false, settings );
        } catch ( MailSizeExceededException ex ) {
            // Try sending a light email
            try {
                renderAndSendMail(emailAddresses, repository, refChange, true, settings );
            } catch ( Throwable t ) {
                // When sending a light mail fails, send a general error mail
                 sendEmail(emailAddresses, "Stash", "Error sending mail for refChange: " + getSubjectPrefix( refChange ) , t.getMessage() );
            }
        }
    }

    private void renderAndSendMail(List<String> emailAddresses, Repository repository, RefChange refChange, boolean renderLightEmail, Settings settings ) {
        String mailBody;
        String fromName;
        String subject;
        try {
            VelocityContext context = new VelocityContext();
            context.put( "name", "World" );
            context.put( "repository", repository );
            context.put( "refChange", refChange );
            context.put( "currentUser", stashAuthenticationContext.getCurrentUser());
            context.put( "smtpNotificationRenderer", smtpNotificationRenderer );
            context.put( "stringEscapeUtils", new StringEscapeUtils() );
            context.put( "applicationProperties", applicationPropertiesService );
            context.put( "serverDomain", getServerDomain() );
            if( !smtpNotificationRenderer.isDeletedBranch( refChange ) && !smtpNotificationRenderer.isCreatedBranch( refChange ) ) {
                // We're not really interested in the number of commits on deleted branches
                context.put( "realNumberOfCommits", getRealNumberOfCommits( repository, refChange ) );
            }
            context.put( "renderLightEmail", renderLightEmail );

            mailBody = renderTemplate(context, defaultTemplate, getOverridenTemplate( settings, "mailOverrideBody", "mailBodyTemplate" ) );
            fromName = renderTemplate(context, defaultFromTemplate, getOverridenTemplate( settings, "mailOverrideFrom", "mailFromTemplate" )  );
            subject = StringUtils.replaceChars(renderTemplate(context, defaultSubjectTemplate, getOverridenTemplate( settings, "mailOverrideSubject", "mailSubjectTemplate" )), "\r\n", "").replaceAll( "( ){2,}", " " );
        } catch( Throwable t ) {
            String fullStack = ExceptionUtils.getFullStackTrace(t);
            mailBody = "<pre>" + t.getMessage() + "\r\n" + fullStack + "</pre>";
            fromName = "notifier@" + getServerDomain();
            subject = getSubjectPrefix( refChange);
        }
        sendEmail( emailAddresses, fromName, subject, mailBody );
    }

    private String renderTemplate(VelocityContext context, String template, String overridenTemplate) throws IOException {
        String mailBody;
        StringWriter sw = new StringWriter();

        Velocity.evaluate(context, sw, "PushEmailNotifier", StringUtils.isNotEmpty( overridenTemplate ) ? overridenTemplate : template );
        mailBody = sw.toString();
        mailBody = StringUtils.trim(mailBody.replaceAll(">\\s*<", ">\r\n<"));
        return mailBody;
    }


    private void sendEmail(List<String> emailAddresses, String fromName, String subject, String mailBody) {
        smtpNotificationRenderer.sendMail( emailAddresses, fromName, subject, mailBody);
    }

    private int getRealNumberOfCommits(Repository repository, RefChange refChange ) {
        GitStringOutputHandler stringOutputHandler = new GitStringOutputHandler();
        GitCommand<String> gitCommand = gitCommandBuilderFactory.builder( repository ).command( "rev-list" ).argument( refChange.getFromHash() + ".." + refChange.getToHash() ).argument( "--count" ).build(stringOutputHandler);
        String output = gitCommand.call();
        if( output != null) {
            return Integer.parseInt( output.replaceAll("(\\t|\\r?\\n)+", "") );
        }
        return 0;
    }

    private String getSubjectPrefix(RefChange refChange) {
        return "[" + smtpNotificationRenderer.getShortHash( refChange.getFromHash() ) + ".." + smtpNotificationRenderer.getShortHash( refChange.getToHash() ) + "@" + refChange.getRefId() + "]";
    }

    private String getServerDomain() {
        String from = applicationPropertiesService.getServerEmailAddress();
        if( from == null ) {
            return "localhost.local";
        } else {
            String[] serverDomain = from.split("@");
            return serverDomain.length > 1 ? serverDomain[1] : serverDomain[0];
        }
    }

    public static String readTextFile( InputStream is, String encoding ) throws IOException {
        StringBuilder sb = new StringBuilder( 1024 );
        BufferedReader reader = null;
        try {
            reader = new BufferedReader( new InputStreamReader( is, encoding ) );

            int reads = 0;
            char[] chars = new char[1024];
            int numRead;
            while ( (numRead = reader.read( chars )) > -1 ) {
                if( reads++ == 0 && ( int ) chars[0] == 65279 ) {
                    // Skip BOM character
                    sb.append( String.valueOf( chars, 1, numRead - 1 ) );
                } else {
                    sb.append( String.valueOf( chars, 0, numRead ) );
                }
            }
        } finally {
            IOUtils.closeQuietly(reader);
        }

        return sb.toString();
    }

    public String getOverridenTemplate( Settings settings, String radio, String template ) {
        if( settings != null && settings.getBoolean( radio, false ) ) {
            return settings.getString( template );
        }
        return null;
    }

    @Override
    public void validate(Settings settings, SettingsValidationErrors errors, Repository repository) {
        if (settings.getString("mailToAddress", "").isEmpty()) {
            errors.addFieldError("mailToAddress", "Please provide an email address");
        }

        validateTemplate(settings, errors, "mailOverrideFrom", "mailFromTemplate" );
        validateTemplate(settings, errors, "mailOverrideSubject", "mailSubjectTemplate" );
        validateTemplate(settings, errors, "mailOverrideBody", "mailBodyTemplate" );
    }

    private void validateTemplate(Settings settings, SettingsValidationErrors errors, String overrideTemplateRadio, String mailTemplate ) {
        boolean overrideTemplate = settings.getBoolean( overrideTemplateRadio, false );

        if( overrideTemplate ) {
            String mailTemplateSettings = settings.getString( mailTemplate, "" );
            if ( mailTemplateSettings.isEmpty() ) {
                errors.addFieldError( mailTemplate, "Please provide a template if you wish to override it" );
            } else {
                try {
                    VelocityContext context = new VelocityContext();
                    renderTemplate(context, mailTemplateSettings, mailTemplateSettings );
                } catch (VelocityException e) {
                    errors.addFieldError( mailTemplate, e.getMessage() );
                } catch ( IOException io ) {
                    errors.addFieldError( mailTemplate, "Cannot evaluate template" + io.getMessage() );
                }
            }
        }
    }
}
