define('notifier/notifier', [
    'exports',
    'jquery',
    'util/events'
], function(
    exports,
    $,
    events
    ) {

    exports.onReady = function(currentBranchId) {
        //TODO: clean up this copy/pasting a bit
        var overrideFromNok = $('#override-from-nok');
        overrideFromNok.change(function() {
            exports.enableOrDisable( $('#mailFromTemplate'), $(this).is(":checked") );
        });
        var overrideFromOk = $('#override-from-yes');
        overrideFromOk.change(function() {
            exports.enableOrDisable( $('#mailFromTemplate'), !$(this).is(":checked") );
        });
        overrideFromNok.trigger( "change" );

        var overrideSubjectNok = $('#override-subject-nok');
        overrideSubjectNok.change(function() {
            exports.enableOrDisable( $('#mailSubjectTemplate'), $(this).is(":checked") );
        });
        var overrideSubjectOk = $('#override-subject-yes');
        overrideSubjectOk.change(function() {
            exports.enableOrDisable( $('#mailSubjectTemplate'), !$(this).is(":checked") );
        });
        overrideSubjectOk.trigger( "change" );

        var overrideBodyNok = $('#override-body-nok');
        overrideBodyNok.change(function() {
            exports.enableOrDisable( $('#mailBodyTemplate'), $(this).is(":checked") );
        });
        var overrideBodyOk = $('#override-body-yes');
        overrideBodyOk.change(function() {
            exports.enableOrDisable( $('#mailBodyTemplate'), !$(this).is(":checked") );
        });
        overrideBodyNok.trigger( "change" );
    };

    exports.enableOrDisable = function( node, checked ) {
        if( checked ) {
            node.attr("readonly","readonly");
        } else {
            node.removeAttr( "readonly" );
        }

    };
});
