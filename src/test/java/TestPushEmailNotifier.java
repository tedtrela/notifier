import be.foreach.stash.hook.GitStringOutputHandler;
import be.foreach.stash.hook.PushEmailNotifier;
import be.foreach.stash.hook.SmtpNotificationRenderer;
import com.atlassian.stash.content.*;
import com.atlassian.stash.exception.MailSizeExceededException;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.i18n.KeyedMessage;
import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.scm.git.GitScmCommandBuilder;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.user.Person;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageImpl;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestPushEmailNotifier {

    @Mock
    private HistoryService historyService;
    @Mock
    private MailService mailService;
    @Mock
    private NavBuilder navBuilder;
    @Mock
    private GitCommandBuilderFactory gitCommandBuilderFactory;
    @Mock
    private StashAuthenticationContext stashAuthenticationContext;
    @Mock
    private ApplicationPropertiesService applicationPropertiesService;

    @Before
    public void setUp() throws Exception {
        reset(historyService, mailService, navBuilder, gitCommandBuilderFactory, stashAuthenticationContext, applicationPropertiesService);
        when( applicationPropertiesService.getServerEmailAddress() ).thenReturn("test.foreach.be");
    }

    @Test
    public void testDeletedBranch() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, historyService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier( notificationRenderer, stashAuthenticationContext, applicationPropertiesService, gitCommandBuilderFactory);

        Collection<RefChange> refChanges = new ArrayList<RefChange>();
        RefChange refChange = mock( RefChange.class );
        when( refChange.getRefId() ).thenReturn( "refs/heads/branchname" );
        when(refChange.getFromHash()).thenReturn( StringUtils.rightPad( "51zefz15", 40, "a" ) );
        when( refChange.getToHash() ).thenReturn( "0000000000000000000000000000000000000000" );
        refChanges.add( refChange );

        StashUser stashUser = getStashUser( "Test User", "test@email.com" );
        when(stashAuthenticationContext.getCurrentUser()).thenReturn( stashUser );

        setNumberOfRealCommits( "0" );
        notifier.postReceive(getRepositoryHookContext(), refChanges);

        String expectedOutput = PushEmailNotifier.readTextFile( TestPushEmailNotifier.class.getClassLoader().getResourceAsStream( "test_deleted_branch.html" ), "UTF-8" );
        verify( notificationRenderer ).sendMail( anyListOf( String.class ), eq( "repo_59@test.foreach.be" ), eq("[51zefz1..0000000@refs/heads/branchname] Branch deleted"), eq( expectedOutput ) );
    }

    @Test
    public void testCreatedBranch() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, historyService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier( notificationRenderer, stashAuthenticationContext, applicationPropertiesService, gitCommandBuilderFactory);

        Collection<RefChange> refChanges = new ArrayList<RefChange>();
        RefChange refChange = mock( RefChange.class );
        when( refChange.getRefId() ).thenReturn( "refs/heads/branchname" );
        when( refChange.getFromHash() ).thenReturn( "0000000000000000000000000000000000000000" );
        when( refChange.getToHash() ).thenReturn( "51zefz15" );
        refChanges.add( refChange );

        StashUser stashUser = getStashUser( "Test User", "test@email.com" );
        when(stashAuthenticationContext.getCurrentUser()).thenReturn( stashUser );

        setNumberOfRealCommits( "0" );
        notifier.postReceive( getRepositoryHookContext(), refChanges );
        String expectedOutput = PushEmailNotifier.readTextFile( TestPushEmailNotifier.class.getClassLoader().getResourceAsStream( "test_created_branch.html" ), "UTF-8" );
        verify( notificationRenderer ).sendMail( anyListOf( String.class ), eq( "repo_59@test.foreach.be" ), eq("[0000000..51zefz1@refs/heads/branchname] Branch created"), eq( expectedOutput ) );
    }

    @Test
    public void testCreatedBranchWithCommits() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, historyService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier( notificationRenderer, stashAuthenticationContext, applicationPropertiesService, gitCommandBuilderFactory);

        Collection<RefChange> refChanges = new ArrayList<RefChange>();
        RefChange refChange = mock( RefChange.class );
        when( refChange.getRefId() ).thenReturn( "refs/heads/branchname" );
        when( refChange.getFromHash() ).thenReturn( "0000000000000000000000000000000000000000" );
        when( refChange.getToHash() ).thenReturn( "51zefz15" );
        refChanges.add( refChange );

        StashUser stashUser = getStashUser( "Test User", "test@email.com" );
        when(stashAuthenticationContext.getCurrentUser()).thenReturn( stashUser );

        RepositoryHookContext repositoryHookContext = getRepositoryHookContext();

        Collection<Changeset> changesets = new ArrayList<Changeset>();
        for( int i=0; i<5; i++ ) {
            Changeset changeset = getMockedChangeset( i );
            changesets.add( changeset );

            List<String> branches = new ArrayList<String>();
            branches.add("thisbranch");
            if( i == 1 || i == 2 ) {
                // Commit 1 and 2 exist on another branch already, skip them for new branch
                branches.add( "master" );
            }
            doReturn( branches ).when(notificationRenderer).getBranches(repositoryHookContext.getRepository(), changeset);
            doReturn( "http://stash.internal/projects/PROJECT/repos/REPO/commits/" + changeset.getId() ).when( notificationRenderer ).getChangesetUrl( repositoryHookContext.getRepository(), changeset );

            List<MinimalChangeset> parents = new ArrayList<MinimalChangeset>();
            MinimalChangeset parent1 = mock( MinimalChangeset.class );
            doReturn( "http://stash.internal/projects/PROJECT/repos/REPO/commits/parent1" ).when( notificationRenderer ).getMinimalChangesetUrl(repositoryHookContext.getRepository(), parent1);
            MinimalChangeset parent2 = mock( MinimalChangeset.class );
            doReturn( "http://stash.internal/projects/PROJECT/repos/REPO/commits/parent2" ).when( notificationRenderer ).getMinimalChangesetUrl(repositoryHookContext.getRepository(), parent2);

            when( parent1.getDisplayId() ).thenReturn( "parent1" );
            when( parent2.getDisplayId() ).thenReturn( "parent2" );

            parents.add(parent1);
            parents.add( parent2 );

            when( changeset.getParents() ).thenReturn( parents );
        }

        Page<Changeset> changesetPage = new PageImpl<Changeset>( new PageRequestImpl( 0, 10 ), changesets, false );
        when( historyService.getChangesetsBetween( (ChangesetsBetweenRequest) anyObject(), (PageRequest) anyObject() ) ).thenReturn( changesetPage );

        Collection<DetailedChangeset> detailedChangesets = new ArrayList<DetailedChangeset>();
        DetailedChangeset detailedChangeset = mock( DetailedChangeset.class );
        detailedChangesets.add( detailedChangeset );

        Collection<Change> changes = new ArrayList<Change>();

        for( int i = 0; i < 17; i++) {
            Change change = mock( Change.class );
            Path path = mock( Path.class );
            when(path.toString()).thenReturn("some/src/main/java/file" + i + ".java");
            when(change.getType()).thenReturn( i % 2 == 0 ? ChangeType.ADD : ChangeType.DELETE );
            when(change.getPath()).thenReturn( path );
            changes.add(change);
        }

        Page<? extends Change> changePage = new PageImpl<Change>( new PageRequestImpl( 0, 10 ), changes, false );

        doReturn( changePage ).when( detailedChangeset ).getChanges();

        Page<DetailedChangeset> detailedChangesetPage = new PageImpl<DetailedChangeset>( new PageRequestImpl( 0, 10 ), detailedChangesets, false );
        when(historyService.getDetailedChangesets((DetailedChangesetsRequest) anyObject(), (PageRequest) anyObject())).thenReturn(detailedChangesetPage);

        setNumberOfRealCommits( "5" );
        notifier.postReceive(repositoryHookContext, refChanges);
        String expectedOutput = PushEmailNotifier.readTextFile( TestPushEmailNotifier.class.getClassLoader().getResourceAsStream( "test_created_branch_with_commits.html" ), "UTF-8" );
        verify( notificationRenderer ).sendMail( anyListOf( String.class ), eq( "repo_59@test.foreach.be" ), eq("[0000000..51zefz1@refs/heads/branchname] Branch created"), eq( expectedOutput ) );
    }

    @Test
    public void testPushWithOneCommit() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, historyService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier( notificationRenderer, stashAuthenticationContext, applicationPropertiesService, gitCommandBuilderFactory);

        Collection<RefChange> refChanges = new ArrayList<RefChange>();
        RefChange refChange = mock( RefChange.class );
        when( refChange.getRefId() ).thenReturn( "refs/heads/branchname" );
        when(refChange.getFromHash()).thenReturn( StringUtils.rightPad("889218518", 40, "e") );
        when( refChange.getToHash() ).thenReturn( StringUtils.rightPad("9999", 40, "f") );
        refChanges.add( refChange );

        StashUser stashUser = getStashUser( "Test User", "test@email.com" );
        when(stashAuthenticationContext.getCurrentUser()).thenReturn( stashUser );

        Collection<Changeset> changesets = new ArrayList<Changeset>();

        Changeset changeset = mock( Changeset.class );
        when(changeset.getMessage()).thenReturn( "Unique      <pre>     message\r\n        with new line\r\n\r\n\r\n      \r\n\r\n   last line    " );
        when(changeset.getId()).thenReturn( StringUtils.rightPad( "1253456", 40, "0" ) );
        changesets.add(changeset);

        Page<Changeset> changesetPage = new PageImpl<Changeset>( new PageRequestImpl( 0, 10 ), changesets, false );
        when(historyService.getChangesetsBetween((ChangesetsBetweenRequest) anyObject(), (PageRequest) anyObject())).thenReturn(changesetPage);

        Collection<DetailedChangeset> detailedChangesets = new ArrayList<DetailedChangeset>();
        DetailedChangeset detailedChangeset = mock( DetailedChangeset.class );
        detailedChangesets.add( detailedChangeset );

        Page<DetailedChangeset> detailedChangesetPage = new PageImpl<DetailedChangeset>( new PageRequestImpl( 0, 10 ), detailedChangesets, false );
        when(historyService.getDetailedChangesets((DetailedChangesetsRequest) anyObject(), (PageRequest) anyObject())).thenReturn(detailedChangesetPage);

        RepositoryHookContext repositoryHookContext = getRepositoryHookContext();
        doReturn( "http://stash.internal/projects/PROJECT/repos/REPO/commits/" + changeset.getId() ).when( notificationRenderer ).getChangesetUrl( repositoryHookContext.getRepository(), changeset );

        setNumberOfRealCommits( "1" );
        notifier.postReceive( repositoryHookContext, refChanges );

        String expectedOutput = PushEmailNotifier.readTextFile( TestPushEmailNotifier.class.getClassLoader().getResourceAsStream( "test_pushed_branch_1_commit.html" ), "UTF-8" );
        verify( notificationRenderer ).sendMail( anyListOf( String.class ), eq( "repo_59@test.foreach.be" ), eq("[9999fff@refs/heads/branchname] Unique <pre> message with new line last line"), eq( expectedOutput ) );
    }

    @Test
    public void TestMailSizeExceededThrowsException() throws IOException {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, historyService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier( notificationRenderer, stashAuthenticationContext, applicationPropertiesService, gitCommandBuilderFactory);

        StashUser stashUser = getStashUser( "Test User", "test@email.com" );
        when(stashAuthenticationContext.getCurrentUser()).thenReturn( stashUser );

        final String expectedFrom = "repo_59@test.foreach.be";
        final String expectedSubject = "[0000000..9999fff@refs/heads/branchname] Branch created";

        KeyedMessage keyedMessage = new KeyedMessage( "key", "localized", "Message size exceed 451651 bytes" );
        doThrow( new MailSizeExceededException( keyedMessage ) ).when(mailService).submit( argThat( new MailMessageBodyContainsArgumentMatcher( expectedFrom, expectedSubject, "<h3>Modified path(s)</h3>") ) );

        Collection<RefChange> refChanges = new ArrayList<RefChange>();
        RefChange refChange = mock( RefChange.class );
        when( refChange.getRefId() ).thenReturn( "refs/heads/branchname" );
        when(refChange.getFromHash()).thenReturn( StringUtils.rightPad("0", 40, "0") );
        when( refChange.getToHash() ).thenReturn( StringUtils.rightPad("9999", 40, "f") );
        refChanges.add( refChange );

        Collection<Changeset> changesets = new ArrayList<Changeset>();

        Changeset changeset = mock( Changeset.class );
        when(changeset.getMessage()).thenReturn( "Test with lots of commits should throw exception" );
        when(changeset.getId()).thenReturn( StringUtils.rightPad( "1253456", 40, "0" ) );
        changesets.add(changeset);

        Page<Changeset> changesetPage = new PageImpl<Changeset>( new PageRequestImpl( 0, 10 ), changesets, false );
        when(historyService.getChangesetsBetween((ChangesetsBetweenRequest) anyObject(), (PageRequest) anyObject())).thenReturn(changesetPage);

        Collection<DetailedChangeset> detailedChangesets = new ArrayList<DetailedChangeset>();
        DetailedChangeset detailedChangeset = mock( DetailedChangeset.class );
        detailedChangesets.add( detailedChangeset );

        Page<DetailedChangeset> detailedChangesetPage = new PageImpl<DetailedChangeset>( new PageRequestImpl( 0, 10 ), detailedChangesets, false );
        when(historyService.getDetailedChangesets((DetailedChangesetsRequest) anyObject(), (PageRequest) anyObject())).thenReturn(detailedChangesetPage);

        RepositoryHookContext repositoryHookContext = getRepositoryHookContext();
        doReturn( "http://stash.internal/projects/PROJECT/repos/REPO/commits/" + changeset.getId() ).when( notificationRenderer ).getChangesetUrl( repositoryHookContext.getRepository(), changeset );

        setNumberOfRealCommits( "156161" );

        try {
            notifier.postReceive( repositoryHookContext, refChanges );
        } catch ( MailSizeExceededException ex ) {
            // Ignore exception here
        }

        ArgumentCaptor<String> fromCaptor = ArgumentCaptor.forClass( String.class );
        ArgumentCaptor<String> subjectCaptor = ArgumentCaptor.forClass( String.class );
        ArgumentCaptor<String> bodyCaptor = ArgumentCaptor.forClass( String.class );
        verify( notificationRenderer, times(2) ).sendMail( anyListOf( String.class ), fromCaptor.capture(), subjectCaptor.capture(), bodyCaptor.capture() );

        List<String> fromNames = fromCaptor.getAllValues();
        List<String> subjects = subjectCaptor.getAllValues();
        List<String> bodies = bodyCaptor.getAllValues();

        for( String fromName : fromNames ) {
            assertEquals( expectedFrom, fromName );
        }

        for( String subject : subjects ) {
            assertEquals( expectedSubject, subject );
        }

        String expectedOutput = PushEmailNotifier.readTextFile( TestPushEmailNotifier.class.getClassLoader().getResourceAsStream( "test_created_branch_too_large.html" ), "UTF-8" );
        String expectedOutputLightMail = PushEmailNotifier.readTextFile( TestPushEmailNotifier.class.getClassLoader().getResourceAsStream( "test_created_branch_too_large_light.html" ), "UTF-8" );
        assertEquals( expectedOutput, bodies.get( 0 ) );
        assertEquals( expectedOutputLightMail, bodies.get( 1 ) );
    }


    private Changeset getMockedChangeset( int changeSetId ) {
        String changeSet = StringUtils.leftPad( Integer.toString( changeSetId ), 40, (char) ( changeSetId + 97 ) );
        Changeset changeset = mock( Changeset.class );
        when(changeset.getId()).thenReturn( changeSet );
        Person author = new Person() {
            @Override
            public String getEmailAddress() {
                return "testuser@someone.com";
            }

            @Override
            public String getName() {
                return "Test User Commit";
            }
        };
        when(changeset.getAuthor()).thenReturn( author);
        Calendar cal = Calendar.getInstance();
        cal.set( 2010, Calendar.NOVEMBER, 30, 10, 15, 40 );
        Date authorTimestamp = cal.getTime();
        when(changeset.getAuthorTimestamp()).thenReturn( authorTimestamp );
        when( changeset.getMessage() ).thenReturn( "Commit message for test case " + changeSet + " remove <li> from html files" );
        return changeset;
    }


    private RepositoryHookContext getRepositoryHookContext() {
        Repository repository = mock( Repository.class );
        when( repository.getId() ).thenReturn( 59 );
        Settings settings = mock( Settings.class );
        when( settings.getString( "mailToAddress" ) ).thenReturn("nobody@there.com");
        return new RepositoryHookContext( repository, settings );
    }

    private StashUser getStashUser( String displayName, String emailAddress ) {
        StashUser user = mock( StashUser.class );
        when( user.getDisplayName() ).thenReturn( displayName );
        when( user.getEmailAddress() ).thenReturn( emailAddress );
        return user;
    }

    private void setNumberOfRealCommits( String numberOfCommits ) {
        GitScmCommandBuilder gitScmCommandBuilder = mock( GitScmCommandBuilder.class );
        when(gitScmCommandBuilder.command( anyString() )).thenReturn( gitScmCommandBuilder );
        when(gitScmCommandBuilder.argument(anyString())).thenReturn( gitScmCommandBuilder );
        GitCommand<String> gitCommand = mock( GitCommand.class );
        when(gitCommand.call()).thenReturn( numberOfCommits );
        when(gitScmCommandBuilder.build(any(GitStringOutputHandler.class))).thenReturn( gitCommand );

        when(gitCommandBuilderFactory.builder(any(Repository.class))).thenReturn(gitScmCommandBuilder);
    }

    private class MailMessageBodyContainsArgumentMatcher extends ArgumentMatcher<MailMessage> {

        private final String expectedFrom;
        private final String expectedSubject;
        private final String expectedBodyContains;

        private MailMessageBodyContainsArgumentMatcher(String expectedFrom, String excpectedSubject, String expectedBodyContains) {
            this.expectedFrom = expectedFrom;
            this.expectedSubject = excpectedSubject;
            this.expectedBodyContains = expectedBodyContains;
        }

        @Override
        public boolean matches(Object argument) {
            if( argument instanceof MailMessage ) {
                MailMessage mailMessage = (MailMessage) argument;
                return expectedFrom != null && expectedSubject != null && expectedFrom.equals(mailMessage.getFrom()) && expectedSubject.equals(mailMessage.getSubject()) && mailMessage.getText().contains( expectedBodyContains );
            } else {
                return false;
            }
        }
    }
}
